package np.com.sahsantosh.civilpatrol.common;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import np.com.sahsantosh.civilpatrol.R;
import np.com.sahsantosh.civilpatrol.api.responses.Crime;

/**
 * Created by santosh on 1/4/18.
 */

public class Utils {
    public static void writeCachedFile (Context context, String key,Object fileName) throws IOException {
        FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(fileName);
        oos.close();
        fos.close();
    }

    public static Object readCachedCrimes(Context context, String key) throws IOException,
            ClassNotFoundException {
        FileInputStream fis = context.openFileInput(key);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return ois.readObject();
    }


    public static int isConnected() {
        Context context = ApplicationClass.context;
        final ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            return 2;
        } else if (mobile.isConnectedOrConnecting()) {
            return 1;
        } else {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return 0;
        }
    }

    public static void requestFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static Date convertStringToDate(String date){
        Date theDate = new Date();
        try{
            theDate =  ApplicationClass.dateFormat.parse(date);
        }catch (ParseException e){
            Toast.makeText(ApplicationClass.context,ApplicationClass.context.getString(R.string.some_error),Toast.LENGTH_SHORT).show();
        }
        return theDate;

    }

}
