package np.com.sahsantosh.civilpatrol.api;

import java.util.List;

import np.com.sahsantosh.civilpatrol.api.responses.Crime;
import np.com.sahsantosh.civilpatrol.models.CrimeType;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by santosh on 1/4/18.
 */

public interface ApiService {
    @GET("home")
    Call<List<CrimeType>> getAllData();

    @GET("crimes")
    Call<List<Crime>> getAllCrimes();
}
