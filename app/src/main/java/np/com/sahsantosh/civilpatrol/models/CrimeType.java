package np.com.sahsantosh.civilpatrol.models;

import android.provider.BaseColumns;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by santosh on 1/3/18.
 */
@Table(name = "crime_types", id = BaseColumns._ID)
public class CrimeType extends Model {

    @Expose
    @Column(name = "id",unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String id;

    @Expose
    @Column(name = "name")
    public String name;


    public CrimeType(){
        super();
    }

    public CrimeType(String id, String name){
        this.id = id;
        this.name = name;
    }

    public static List<CrimeType> getAllCrimeType(){
        return new Select()
                .all()
                .from(CrimeType.class)
                .execute();
    }

    public static void saveAllCrimeTypes(List<CrimeType> crimeTypes){
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i<crimeTypes.size(); i++){
                CrimeType  crimeType = crimeTypes.get(i);
                crimeType.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        }finally {
            ActiveAndroid.endTransaction();
        }
    }
}
