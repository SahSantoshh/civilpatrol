package np.com.sahsantosh.civilpatrol.api.responses;

import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by santosh on 1/4/18.
 */

public class Crime implements Serializable {
    @Expose
    public String id;

    @Expose
    public double lat;

    @Expose
    public double lng;

    @Expose
    public int solved;

    @Expose
    public String avatar_file_name;

    @Expose
    public String title;

    @Expose
    public String description;

    @Expose
    public String created_at;

    @Expose
    public String updated_at;
}
