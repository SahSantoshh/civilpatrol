package np.com.sahsantosh.civilpatrol.splash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import java.util.List;

import np.com.sahsantosh.civilpatrol.R;
import np.com.sahsantosh.civilpatrol.api.ApiService;
import np.com.sahsantosh.civilpatrol.api.ApiUtil;
import np.com.sahsantosh.civilpatrol.common.Utils;
import np.com.sahsantosh.civilpatrol.main.MainActivity;
import np.com.sahsantosh.civilpatrol.models.CrimeType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    public static final int Requestcode = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        int connected = Utils.isConnected();

        if (Build.VERSION.SDK_INT >= 23){
            if (checkPermission()){
                if (connected > 0){
                    getAllData();
                }else {
                    goToMainActivity();
                }
            } else {
                requestPermission(); // Code for permission
            }
        }else{
            if (connected > 0){
                getAllData();
            }else {
                goToMainActivity();
            }
        }
    }

    public void getAllData(){
        ApiService apiService = ApiUtil.getApiService();
        apiService.getAllData().enqueue(new Callback<List<CrimeType>>() {
            @Override
            public void onResponse(Call<List<CrimeType>> call, Response<List<CrimeType>> response) {
                if (response.isSuccessful()){
                    CrimeType.saveAllCrimeTypes(response.body());
                    goToMainActivity();
                }else {
                    Toast.makeText(getApplicationContext(),"Some Error",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<CrimeType>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Failed to fetch",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Requestcode:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAllData();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.INTERNET,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, Requestcode);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}
