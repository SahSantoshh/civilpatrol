package np.com.sahsantosh.civilpatrol.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import np.com.sahsantosh.civilpatrol.R;
import np.com.sahsantosh.civilpatrol.api.ApiService;
import np.com.sahsantosh.civilpatrol.api.ApiUtil;
import np.com.sahsantosh.civilpatrol.api.responses.Crime;
import np.com.sahsantosh.civilpatrol.common.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static np.com.sahsantosh.civilpatrol.common.Utils.readCachedCrimes;
import static np.com.sahsantosh.civilpatrol.common.Utils.writeCachedFile;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView crimeRecyclre;
    List<Crime> crimes;
    TextView noInternet;
    private int backButtonCount = 0;
    CrimeAdapter crimeAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab =  findViewById(R.id.fab);
        noInternet = findViewById(R.id.no_internet_layout);
        int connected = Utils.isConnected();

        if (connected>0){
            noInternet.setVisibility(View.GONE);
            getCrimes();
        }else {
            noInternet.setVisibility(View.VISIBLE);
            getCacheCrimes();
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Comming Soon",Toast.LENGTH_LONG).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (backButtonCount >= 1){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getCrimes(){
        ApiService apiService = ApiUtil.getApiService();
        apiService.getAllCrimes().enqueue(new Callback<List<Crime>>() {
            @Override
            public void onResponse(Call<List<Crime>> call, Response<List<Crime>> response) {
                if (response.isSuccessful()){
                    crimes = response.body();
                    setCacheCrimes();
                }else {
                    Toast.makeText(getApplicationContext(),"Some Error",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Crime>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Some Error",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addNewCrimes(List<Crime> newCrimes){
        crimes.clear();
        crimes.addAll(newCrimes);
        crimeAdapter.notifyDataSetChanged();
    }

    private void setCacheCrimes() {
        try{
            writeCachedFile(this,"crime",crimes);
        }catch (IOException e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
        }
        setAdapter();
    }

    private void getCacheCrimes() {
        try {
            crimes = (List<Crime>) readCachedCrimes(this,"crime");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        setAdapter();
    }

    private void setAdapter() {
        crimeRecyclre = findViewById(R.id.main_crimes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        crimeRecyclre.setLayoutManager(layoutManager);
        crimeAdapter = new CrimeAdapter(MainActivity.this,crimes);
        crimeRecyclre.setAdapter(crimeAdapter);
        crimeRecyclre.setItemAnimator(new DefaultItemAnimator());

    }


}
