package np.com.sahsantosh.civilpatrol.common;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by santosh on 1/3/18.
 */

public class ApplicationClass extends Application {
    public static Context context;
    public static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);

    public static final SimpleDateFormat simpleDateFormat =
            new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    public void onCreate(){
        super.onCreate();
        context = getApplicationContext();

        Configuration dbConfig = new Configuration.Builder(this).setDatabaseName("CivilPatrol.db").create();
        ActiveAndroid.initialize(dbConfig);
    }
}
