package np.com.sahsantosh.civilpatrol.main;

import android.app.Application;
import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import np.com.sahsantosh.civilpatrol.R;
import np.com.sahsantosh.civilpatrol.api.ApiUtil;
import np.com.sahsantosh.civilpatrol.api.responses.Crime;
import np.com.sahsantosh.civilpatrol.common.ApplicationClass;
import np.com.sahsantosh.civilpatrol.common.Utils;

/**
 * Created by santosh on 1/4/18.
 */

public class CrimeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    List<Crime> crimes;
    private static final int ITEMS = 0;
    private static final int NO_ITEMS = 1;



    public CrimeAdapter(Context context, List<Crime> crimes) {
        this.context = context;
        this.crimes = crimes;
    }

    public class CrimeViewHolder extends RecyclerView.ViewHolder {
        CardView crimeCard;
        TextView crimeTitle,crimeLocation,crimeDate,crimeDesc;
        ImageView crimePhoto;

        public CrimeViewHolder(View itemView) {
            super(itemView);
            crimeCard = itemView.findViewById(R.id.crime_card);
            crimeTitle = itemView.findViewById(R.id.crime_title);
            crimeLocation = itemView.findViewById(R.id.crime_location);
            crimeDate = itemView.findViewById(R.id.crime_date);
            crimeDesc = itemView.findViewById(R.id.crime_desc);
            crimePhoto = itemView.findViewById(R.id.crime_photo);

        }
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder{
        TextView noCrimes;
        public EmptyViewHolder(View itemView) {
            super(itemView);
            noCrimes = itemView.findViewById(R.id.no_crimes);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CrimeViewHolder){
            Collections.reverse(this.crimes);
            Crime crime = crimes.get(position);
            if (crime.title != null){
                ((CrimeViewHolder)holder).crimeTitle.setText(crime.title);
            }else {
                ((CrimeViewHolder)holder).crimeTitle.setText("Show Title");
            }

            if (crime.description != null){
                ((CrimeViewHolder)holder).crimeDesc.setText(crime.description);
            }else {
                ((CrimeViewHolder)holder).crimeDesc.setVisibility(View.GONE);
            }

            ((CrimeViewHolder)holder).crimeLocation.setText("Show Location");

            Date date = Utils.convertStringToDate(crime.created_at);
            String cDate = ApplicationClass.simpleDateFormat.format(date);
            ((CrimeViewHolder)holder).crimeDate.setText(cDate);


            if (crime.avatar_file_name!= null){
                String filePath = ApiUtil.base_URl + "crime/"+crime.id+"/"+crime.avatar_file_name;
                Glide.with(context).load(filePath).into(((CrimeViewHolder) holder).crimePhoto);
            }else {
                Glide.with(context).load(R.mipmap.ic_launcher).into(((CrimeViewHolder) holder).crimePhoto);
            }



        }
        if (holder instanceof  EmptyViewHolder){
            ((EmptyViewHolder) holder).noCrimes.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEMS){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.one_crime,parent,false);
            return new CrimeViewHolder(view);
        }
        if (viewType == NO_ITEMS){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_crimes,parent,false);
            return new EmptyViewHolder(view);
        }
        return null;
    }



    @Override
    public int getItemCount() {
        if (crimes != null){
            return this.crimes.size();
        }else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        int size = this.crimes.size();
        if (size > 0){
            return ITEMS;
        }else {
            return NO_ITEMS;
        }

    }



}
