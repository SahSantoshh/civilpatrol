package np.com.sahsantosh.civilpatrol.api;

/**
 * Created by santosh on 1/4/18.
 */

public class ApiUtil {
    //    public  static String base_URl = "http://192.168.100.49:3000/"; //office
    public  static String base_URl = "http://192.168.1.10:3000/"; //room
//        public  static String base_URl = "https://crimepatrol.herokuapp.com/";
    public static String version = "api/v1/";

    public static ApiService getApiService() {

        return Client.getClient(base_URl+version).create(ApiService.class);
    }
}
